Assignments
===========

This is the [CSE-30872-FA17] reading and challenge assignments repository for:

- **Name**:     Domer McDomerson

- **NetID**:    dmcdomer

[CSE-30872-FA17]:   https://www3.nd.edu/~pbui/teaching/cse.30872.fa17/

Readings
--------

**Readings** are due **before** class on the **Monday** of each week and are
each worth **2** points.

- [Reading 00](https://www3.nd.edu/~pbui/teaching/cse.30872.fa17/reading00.html)

- [Reading 01](https://www3.nd.edu/~pbui/teaching/cse.30872.fa17/reading01.html)

- [Reading 02](https://www3.nd.edu/~pbui/teaching/cse.30872.fa17/reading02.html)

- [Reading 03](https://www3.nd.edu/~pbui/teaching/cse.30872.fa17/reading03.html)

- [Reading 04](https://www3.nd.edu/~pbui/teaching/cse.30872.fa17/reading04.html)

- [Reading 05](https://www3.nd.edu/~pbui/teaching/cse.30872.fa17/reading05.html)

- [Reading 06](https://www3.nd.edu/~pbui/teaching/cse.30872.fa17/reading06.html)

- [Reading 07](https://www3.nd.edu/~pbui/teaching/cse.30872.fa17/reading07.html)

- [Reading 08](https://www3.nd.edu/~pbui/teaching/cse.30872.fa17/reading08.html)

- [Reading 09](https://www3.nd.edu/~pbui/teaching/cse.30872.fa17/reading09.html)

- [Reading 10](https://www3.nd.edu/~pbui/teaching/cse.30872.fa17/reading10.html)

- [Reading 11](https://www3.nd.edu/~pbui/teaching/cse.30872.fa17/reading11.html)

Challenges
----------

**Challenges** are due **before** class on the **Friday** of each week. This is
a *soft* deadline for the first attempt. Final submissions are due at **noon**
on the **Saturday** of each week.  Each **Challenge** is worth **6** points.

- [Challenge 00](https://www3.nd.edu/~pbui/teaching/cse.30872.fa17/challenge00.html)

- [Challenge 01](https://www3.nd.edu/~pbui/teaching/cse.30872.fa17/challenge01.html)

- [Challenge 02](https://www3.nd.edu/~pbui/teaching/cse.30872.fa17/challenge02.html)

- [Challenge 03](https://www3.nd.edu/~pbui/teaching/cse.30872.fa17/challenge03.html)

- [Challenge 04](https://www3.nd.edu/~pbui/teaching/cse.30872.fa17/challenge04.html)

- [Challenge 05](https://www3.nd.edu/~pbui/teaching/cse.30872.fa17/challenge05.html)

- [Challenge 06](https://www3.nd.edu/~pbui/teaching/cse.30872.fa17/challenge06.html)

- [Challenge 07](https://www3.nd.edu/~pbui/teaching/cse.30872.fa17/challenge07.html)

- [Challenge 08](https://www3.nd.edu/~pbui/teaching/cse.30872.fa17/challenge08.html)

- [Challenge 09](https://www3.nd.edu/~pbui/teaching/cse.30872.fa17/challenge09.html)

- [Challenge 10](https://www3.nd.edu/~pbui/teaching/cse.30872.fa17/challenge10.html)

- [Challenge 11](https://www3.nd.edu/~pbui/teaching/cse.30872.fa17/challenge11.html)

- [Challenge 12](https://www3.nd.edu/~pbui/teaching/cse.30872.fa17/challenge12.html)

- [Challenge 13](https://www3.nd.edu/~pbui/teaching/cse.30872.fa17/challenge13.html)

- [Challenge 14](https://www3.nd.edu/~pbui/teaching/cse.30872.fa17/challenge14.html)

- [Challenge 15](https://www3.nd.edu/~pbui/teaching/cse.30872.fa17/challenge15.html)

- [Challenge 16](https://www3.nd.edu/~pbui/teaching/cse.30872.fa17/challenge16.html)

- [Challenge 17](https://www3.nd.edu/~pbui/teaching/cse.30872.fa17/challenge17.html)

- [Challenge 18](https://www3.nd.edu/~pbui/teaching/cse.30872.fa17/challenge18.html)

- [Challenge 19](https://www3.nd.edu/~pbui/teaching/cse.30872.fa17/challenge19.html)

- [Challenge 20](https://www3.nd.edu/~pbui/teaching/cse.30872.fa17/challenge20.html)

- [Challenge 21](https://www3.nd.edu/~pbui/teaching/cse.30872.fa17/challenge21.html)

- [Challenge 22](https://www3.nd.edu/~pbui/teaching/cse.30872.fa17/challenge22.html)

- [Challenge 23](https://www3.nd.edu/~pbui/teaching/cse.30872.fa17/challenge23.html)
